alter table accounts alter column currency type varchar(32);
alter table accounts alter column language type varchar(32);
alter table webhooks alter column status type varchar(32);