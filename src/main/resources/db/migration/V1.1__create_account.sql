CREATE TABLE IF NOT EXISTS accounts(
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid(),
    amount decimal(12, 2),
    currency currency_type,
    language language_type,
    merchant_uid VARCHAR(36)
)