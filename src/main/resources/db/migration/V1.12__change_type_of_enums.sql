alter table transactions alter column currency type varchar(32);
alter table transactions alter column language type varchar(32);
alter table transactions alter column transactions_type type varchar(32);