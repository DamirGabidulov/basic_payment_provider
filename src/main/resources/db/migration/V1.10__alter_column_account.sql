alter table if exists cards
    add column if not exists balance decimal(12, 2) default 0;