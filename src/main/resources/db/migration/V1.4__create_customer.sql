CREATE TABLE IF NOT EXISTS customers(
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid(),
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    country VARCHAR(128)
);