CREATE TABLE webhooks (
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid(),
    notification_url VARCHAR(256),
    status status_type,
    attempt_number INTEGER,
    created_at TIMESTAMP,
    request_body varchar(256),
    response_body varchar(256),
    transaction_uid VARCHAR(36),
    CONSTRAINT fk_webhook_transaction
    FOREIGN KEY (transaction_uid) REFERENCES transactions(id)
)