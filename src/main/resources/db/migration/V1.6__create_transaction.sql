CREATE TABLE IF NOT EXISTS transactions(
    id VARCHAR(36) PRIMARY KEY DEFAULT  gen_random_uuid(),
    payment_method VARCHAR(32),
    amount DECIMAL,
    currency currency_type,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    language language_type,
    notification_url VARCHAR(256),
    status status_type,
    transactions_type  transaction_type,
    card_uid BIGINT,
    CONSTRAINT fk_transactions_cards
    FOREIGN KEY (card_uid) REFERENCES cards(id),
    account_uid VARCHAR(36),
    CONSTRAINT fk_transactions_accounts
    FOREIGN KEY (account_uid) REFERENCES accounts (id)
);