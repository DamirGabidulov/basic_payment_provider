package ru.provider.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.provider.entity.Status;
import ru.provider.entity.Transaction;

public interface TransactionRepository  extends R2dbcRepository<Transaction, String> {
    @Query("select * from transactions where status = :status limit :size")
    Flux<Transaction> findByStatus(Status status, int size);

    @Query("select * from transactions where id = :id for update")
    Mono<Transaction> findByIdForUpdate(String id);
}
