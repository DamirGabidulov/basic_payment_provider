package ru.provider.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.provider.entity.Account;
import ru.provider.entity.Currency;
import ru.provider.entity.Transaction;

public interface AccountRepository extends R2dbcRepository<Account, String> {

//    @Query("select * from accounts where currency = :currency::currency_type")
    @Query("select * from accounts where merchant_uid = :merchantId and currency = :currency")
    Mono<Account> getByMerchantUidAndCurrency(String merchantId, Currency currency);

    @Query("select * from accounts where id = :id for update")
    Mono<Account> findByIdForUpdate(String id);

}
