package ru.provider.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import ru.provider.entity.Webhook;

public interface WebhookRepository extends R2dbcRepository<Webhook, String> {
}
