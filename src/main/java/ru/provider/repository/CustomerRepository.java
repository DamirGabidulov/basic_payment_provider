package ru.provider.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import ru.provider.entity.Customer;

public interface CustomerRepository extends R2dbcRepository<Customer, String> {

}
