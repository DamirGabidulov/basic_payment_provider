package ru.provider.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import ru.provider.entity.Merchant;

public interface MerchantRepository extends R2dbcRepository<Merchant, String> {


}
