package ru.provider.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.provider.entity.Card;

public interface CardRepository extends R2dbcRepository<Card, Long> {

    @Query("select * from cards where card_number = $1")
    Mono<Card> findByCardNumber(Long cardNumber);

    @Query("select * from cards where id = $1 for update")
    Mono<Card> findByIdForUpdate(Long cardId);
}
