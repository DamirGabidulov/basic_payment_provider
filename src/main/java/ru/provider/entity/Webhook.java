package ru.provider.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table("webhooks")
public class Webhook implements Persistable<String> {

    @Id
    private String id;
    @Column("notification_url")
    private String notificationUrl;
    @Column("status")
    private Status status;
    @Column("attempt_number")
    private Integer attemptNumber;
    @Column("created_at")
    private LocalDateTime createdAt;
    //TODO СПРОСИТЬ может стоит убрать строку с реквестом, потому что DepositRequestDto на проведение транзакции это отдельный флоу
    @Column("request_body")
    private String requestBody;
    //TODO СПРОСИТЬ строго говоря и респонс это транзакция, мб лучше просто оставить транзакцию
    @Column("response_body")
    private String responseBody;
    @Column("transaction_uid")
    private String transactionUid;

    @Transient
    private Transaction transaction;

    @Override
    public boolean isNew() {
        return !StringUtils.hasText(id);
    }
}
