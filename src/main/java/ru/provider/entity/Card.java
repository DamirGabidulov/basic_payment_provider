package ru.provider.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table("cards")
public class Card implements Persistable<Long> {

    @Id
    private Long id;
    @Column("card_number")
    private Long cardNumber;
    @Column("expiration_date")
    private String expirationDate;
    @Column("cvv")
    private Integer cvv;
    @Column("balance")
    private BigDecimal balance;

    @Transient
    private Customer customer;
    @Column("customer_uid")
    private String customerUid;

    @Transient
    @ToString.Exclude
    private List<Transaction> transactions;

    @Override
    public boolean isNew() {
        return id==null;
    }
}
