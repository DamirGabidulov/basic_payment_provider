package ru.provider.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("transactions")
public class Transaction implements Persistable<String> {

    @Id
    private String id;
    @Column("payment_method")
    private String paymentMethod;
    @Column("amount")
    private BigDecimal amount;
    @Column("currency")
    private Currency currency;
    @Column("created_at")
    private LocalDateTime createdAt;
    @Column("updated_at")
    private LocalDateTime updatedAt;
    @Column("language")
    private Language language;
    @Column("notification_url")
    private String notificationUrl;
    @Column("status")
    private Status status;
    @Column("transactions_type")
    private TransactionType transactionType;
    @Column("card_uid")
    private Long cardUid;
    @Column("account_uid")
    private String accountUid;

    @Transient
    private Card card;
    @Transient
    private Account account;

    @Override
    public boolean isNew() {
        return !StringUtils.hasText(id);
    }
}
