package ru.provider.entity;

import java.util.Arrays;
import java.util.Random;

public enum Status {

    IN_PROGRESS, SUCCESS, FAILED;

    private static final Status[] STATUSES = new Status[]{SUCCESS, FAILED};
    private static final int SIZE = STATUSES.length;
    private static final Random RANDOM = new Random();

    public static Status getRandomSuccessOrFailedStatus(){
        return STATUSES[RANDOM.nextInt(SIZE)];
    }
}
