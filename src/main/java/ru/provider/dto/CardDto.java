package ru.provider.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import ru.provider.entity.Customer;
import ru.provider.entity.Transaction;

import java.math.BigDecimal;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIgnoreProperties(value = {"id", "customer", "transactions"})
public class CardDto {
    private Long id;
    private Long cardNumber;
    private String expirationDate;
    private Integer cvv;
    private BigDecimal balance;
    private Customer customer;
    private String customerUid;
    private List<Transaction> transactions;
}
