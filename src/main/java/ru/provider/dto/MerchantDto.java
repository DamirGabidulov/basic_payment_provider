package ru.provider.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Column;
import ru.provider.entity.Account;
import ru.provider.entity.Merchant;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MerchantDto {
    private String id;
    private String secretKey;
    @JsonIgnore
    private List<Account> accounts;

    public static MerchantDto from(Merchant merchant){
        return MerchantDto.builder()
                .id(merchant.getId())
                .secretKey(merchant.getSecretKey())
                .accounts(merchant.getAccounts())
                .build();
    }

    public static List<MerchantDto> from(List<Merchant> merchants){
        return merchants.stream().map(MerchantDto::from).collect(Collectors.toList());
    }
}
