package ru.provider.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.provider.entity.Card;
import ru.provider.entity.Currency;
import ru.provider.entity.Language;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class DepositRequestDto {
    private String paymentMethod;
    private BigDecimal amount;
    private Currency currency;
    private CardDto cardData;
    private Language language;
    private String notificationUrl;
    private CustomerDto customer;
}
