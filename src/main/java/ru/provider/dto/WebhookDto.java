package ru.provider.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Column;
import ru.provider.entity.Status;
import ru.provider.entity.Transaction;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WebhookDto {
    private String id;
    //uid это id в бд, так как провайдер mockapi.io проставляет свои id для вебхуков
    private String uid;
    private String notificationUrl;
    private Status status;
    private Integer attemptNumber;
    private LocalDateTime createdAt;
    private String requestBody;
    private String responseBody;
    private String transactionUid;
    private Transaction transaction;
}
