package ru.provider.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.provider.dto.WebhookDto;
import ru.provider.service.TransactionService;
import ru.provider.service.WebClientWebhookService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/webhook/transaction")
public class WebhookController {

    private final WebClientWebhookService webClientWebhookService;
    private final TransactionService transactionService;


    @PostMapping("/webclient")
    public Mono<?> addWebhookViaWebClient(@RequestBody WebhookDto webhookDto) {
        return Mono.just(webhookDto)
                .flatMap(dto -> transactionService.findById("8399557a-92e0-4d10-86e8-cb553d9f01fd")
                        .map(x -> {
                            dto.setTransaction(x);
                            return dto;
                        }))
                .flatMap(data -> webClientWebhookService.addWebhook(data, data.getNotificationUrl()));

    }
}
