package ru.provider.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.provider.dto.AuthResponseDto;
import ru.provider.dto.MerchantDto;
import ru.provider.entity.Merchant;
//import ru.provider.security.SecurityService;
import ru.provider.service.MerchantService;

@Slf4j
@RestController
@RequestMapping("api/v1/merchant")
@RequiredArgsConstructor
public class MerchantController {

    private final MerchantService merchantService;

    @PostMapping
    public Mono<ResponseEntity<MerchantDto>> createMerchant(@RequestBody MerchantDto dto) {
//        return Mono.just(ResponseEntity.of(merchantService.createMerchant(dto).)) ;
        return merchantService.createMerchant(dto).map(ResponseEntity::ok);
    }

//    @PostMapping("/login")
//    public Mono<AuthResponseDto> login(@RequestBody MerchantDto dto) {
//        return securityService.authenticate(dto.getId(), dto.getSecretKey())
//                .flatMap(tokenDetails -> Mono.just(
//                        AuthResponseDto.builder()
//                                .id(tokenDetails.getMerchantId())
//                                .token(tokenDetails.getToken())
//                                .issuedAt(tokenDetails.getIssuedAt())
//                                .expiresAt(tokenDetails.getExpiresAt())
//                                .build()
//                ));
//    }

    @GetMapping("/unsecured/{merchant-id}")
    public Mono<ResponseEntity<MerchantDto>> getById(@PathVariable("merchant-id") String id) {
//        log.info("header name {}", header);
        return merchantService.getMerchantById(id).map(merchant -> ResponseEntity.ok(MerchantDto.from(merchant)));
    }

    @GetMapping("/secured/{merchant-id}")
    public Mono<ResponseEntity<MerchantDto>> securedGetById(@PathVariable("merchant-id") String id, @RequestHeader(HttpHeaders.AUTHORIZATION) String header) {
        log.info("header name {}", header);
        return merchantService.getMerchantById(id).map(merchant -> ResponseEntity.ok(MerchantDto.from(merchant)));
    }



}
