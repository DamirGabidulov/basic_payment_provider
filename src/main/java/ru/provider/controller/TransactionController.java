package ru.provider.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import ru.provider.dto.DepositRequestDto;
import ru.provider.dto.TransactionResponse;
import ru.provider.service.TransactionService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/payments/transaction")
public class TransactionController {

    private final TransactionService transactionService;

    @PostMapping
    public Mono<?> createDepositTransaction(@RequestBody DepositRequestDto requestDto,
                                                                              @RequestHeader(HttpHeaders.AUTHORIZATION) String header,
                                                                              ServerWebExchange exchange) {
        String merchantId = exchange.getAttribute("merchantId");
        log.info("header name {}", header);
        log.info("attribute value {}", merchantId);

        return transactionService.createTransaction(merchantId, requestDto)
                .flatMap(transaction -> Mono.just(new TransactionResponse(transaction.getId(), transaction.getStatus(), "OK")));
    }

}
