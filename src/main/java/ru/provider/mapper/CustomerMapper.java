package ru.provider.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import ru.provider.dto.CardDto;
import ru.provider.dto.CustomerDto;
import ru.provider.entity.Customer;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    CustomerDto map(Customer customer);
    @InheritInverseConfiguration
    Customer map(CustomerDto customerDto);
}
