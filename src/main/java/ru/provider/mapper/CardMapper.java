package ru.provider.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import ru.provider.dto.CardDto;
import ru.provider.entity.Card;

@Mapper(componentModel = "spring")
public interface CardMapper {
    CardDto map(Card card);

    @InheritInverseConfiguration
    Card map(CardDto dto);
}
