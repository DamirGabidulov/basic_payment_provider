package ru.provider.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.provider.dto.WebhookDto;
import ru.provider.entity.Webhook;

@Mapper(componentModel = "spring")
public interface WebhookMapper {

    @Mapping(target = "uid", source = "id")
    WebhookDto map(Webhook webhook);

    @InheritInverseConfiguration
    @Mapping(target = "id", source = "uid")
    Webhook map(WebhookDto webhookDto);
}
