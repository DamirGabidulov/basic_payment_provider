package ru.provider.exception;

import lombok.Getter;

@Getter
public class WebhookTimeoutException extends RuntimeException{
    protected String errorCode;

    public WebhookTimeoutException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}
