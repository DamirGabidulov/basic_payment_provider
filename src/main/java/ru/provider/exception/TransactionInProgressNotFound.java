package ru.provider.exception;

import lombok.Getter;

@Getter
public class TransactionInProgressNotFound extends RuntimeException{
    protected String errorCode;

    public TransactionInProgressNotFound(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}
