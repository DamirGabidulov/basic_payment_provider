package ru.provider.processing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.provider.dto.WebhookDto;
import ru.provider.entity.Status;
import ru.provider.entity.Transaction;
import ru.provider.exception.TransactionInProgressNotFound;
import ru.provider.mapper.WebhookMapper;
import ru.provider.service.AccountService;
import ru.provider.service.TransactionService;
import ru.provider.service.WebClientWebhookService;
import ru.provider.service.WebhookService;

import java.time.LocalDateTime;

@Slf4j
@Component
@RequiredArgsConstructor
public class TransactionCompleteProcessing {

    private final TransactionService transactionService;
    private final WebClientWebhookService webClientWebhookService;
    private final WebhookService webhookService;
    private final WebhookMapper webhookMapper;

    @Scheduled(cron = "0 * * * * *") //каждую минуту
    public void makeTransactionSuccess() {
        transactionService.processing()
                .flatMap(transaction -> {
                    //TODO статус у webhook будет свой
                    WebhookDto dto = createWebhookDto(transaction);
                    return webhookService.save(dto)
                            .flatMap(webhook -> webClientWebhookService.addWebhook(webhookMapper.map(webhook), transaction.getNotificationUrl()));
                })
                .doOnError(TransactionInProgressNotFound.class, ex -> log.warn("processing progress has problem {}", ex.toString()))
                .onErrorResume(TransactionInProgressNotFound.class, ex -> Mono.empty())
                .subscribe(result -> {
                    log.info("complete processing for " + result.toString());
                });
    }

    private static WebhookDto createWebhookDto(Transaction transaction) {
        WebhookDto dto = WebhookDto.builder()
                .notificationUrl(transaction.getNotificationUrl())
                .status(transaction.getStatus())
                .attemptNumber(1)
                .createdAt(LocalDateTime.now())
                .responseBody(transaction.toString())
                .transaction(transaction)
                .transactionUid(transaction.getId())
                .build();
        return dto;
    }
}