package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.provider.dto.CustomerDto;
import ru.provider.entity.Customer;
import ru.provider.mapper.CustomerMapper;
import ru.provider.repository.CustomerRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    public Mono<Customer> save(CustomerDto customerDto){
        return customerRepository.save(customerMapper.map(customerDto));
    }
}
