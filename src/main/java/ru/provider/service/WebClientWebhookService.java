package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.util.retry.Retry;
import ru.provider.dto.WebhookDto;
import ru.provider.entity.Status;
import ru.provider.exception.WebhookTimeoutException;
import ru.provider.mapper.WebhookMapper;

import java.time.Duration;

@Slf4j
@Service
@RequiredArgsConstructor
public class WebClientWebhookService {

    private final WebClient webClient;
    private final WebhookService webhookService;
    private final WebhookMapper webhookMapper;

    public Mono<?> addWebhook(WebhookDto webhookDto, String notificationUrl) {
        return webClient.post()
                .uri(notificationUrl)
                .body(Mono.just(webhookDto), WebhookDto.class)
                .retrieve()
                .bodyToMono(WebhookDto.class)
                //Retry.fixedDelay(4, Duration.ofSeconds(10) будет отдавать каждые 10 секунд, Retry.backoff нарастает по экспоненте
                .retryWhen(Retry.backoff(4, Duration.ofSeconds(10))
                        .filter(this::is5xxServerError)
                        .filter(this::isIncorrectResponse)
                        .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> new WebhookTimeoutException("retries exhausted", "ATTEMPTS ARE OUT OF LIMIT"))
                )
                .doOnSuccess(dto -> {
                    log.info("Webhook sent correctly " + dto.getUid());
                })
                .onErrorResume(e -> {
                    log.error("catch error " + e.getMessage(), e);
                    return Mono.empty();
                } )
                .switchIfEmpty(Mono.defer(() -> {
                    webhookDto.setStatus(Status.FAILED);
                    webhookDto.setAttemptNumber(5);
                    return webhookService.update(webhookMapper.map(webhookDto)).map(webhookMapper::map);
                }))
                ;
    }

    private boolean is5xxServerError(Throwable throwable) {
        return throwable instanceof WebClientResponseException &&
                ((WebClientResponseException) throwable).getStatusCode().is5xxServerError();
    }

    private boolean isIncorrectResponse(Throwable throwable) {
        log.info("FOUND PROBLEM IN WEBHOOK " + throwable.getMessage());
        return throwable instanceof WebClientRequestException;
    }
}
