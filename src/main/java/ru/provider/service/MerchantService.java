package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;
import ru.provider.dto.MerchantDto;
import ru.provider.entity.Merchant;
import ru.provider.repository.MerchantRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class MerchantService {

    private final MerchantRepository merchantRepository;
    private final PasswordEncoder passwordEncoder;

    public Mono<MerchantDto> createMerchant(MerchantDto merchantDto){
        Merchant merchant = Merchant.builder()
                .secretKey(merchantDto.getSecretKey())
                .accounts(merchantDto.getAccounts())
                .build();
        return merchantRepository.save(merchant.toBuilder()
                        .secretKey(passwordEncoder.encode(merchantDto.getSecretKey()))
                .build()
        ).doOnSuccess(u -> {
            log.info("IN registerUser - user: {} created", u);
        }).map(MerchantDto::from);
    }

    public Mono<Merchant> getMerchantById(String id){
        return merchantRepository.findById(id);
    }
}
