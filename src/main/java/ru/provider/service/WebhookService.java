package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.provider.dto.WebhookDto;
import ru.provider.entity.Transaction;
import ru.provider.entity.Webhook;
import ru.provider.mapper.WebhookMapper;
import ru.provider.repository.WebhookRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class WebhookService {

    private final WebhookRepository webhookRepository;
    private final WebhookMapper webhookMapper;

    public Mono<Webhook> save(WebhookDto dto){
        return webhookRepository.save(webhookMapper.map(dto));
    }

    public Mono<Webhook> update(Webhook webhook) {
        return webhookRepository.save(webhook);
    }
}
