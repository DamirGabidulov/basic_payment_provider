package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.provider.dto.DepositRequestDto;
import ru.provider.entity.*;
import ru.provider.exception.TransactionInProgressNotFound;
import ru.provider.repository.TransactionRepository;

import java.sql.SQLException;
import java.time.LocalDateTime;

import static ru.provider.entity.Status.getRandomSuccessOrFailedStatus;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;
    private final CardService cardService;
    private final TransactionalOperator transactionalOperator;
    private final WebClientWebhookService webClientWebhookService;

    public Mono<Transaction> createTransaction(String merchantId, DepositRequestDto requestDto) {
        Mono<Account> account = accountService.getByTypeAndMerchantId(merchantId, requestDto.getCurrency());
        Mono<Card> cardMono = cardService.saveIfNotExists(requestDto.getCardData(), requestDto.getCustomer());
        Mono<String> accountUid = account.mapNotNull(Account::getId);
        return account
                .then(cardService.verify(requestDto.getAmount(), cardMono))
                .flatMap(card -> cardService.updateBalance(requestDto.getAmount(), card))
                .flatMap(card -> save(requestDto, TransactionType.TRANSACTION, card, accountUid));
    }

    public Mono<Transaction> save(DepositRequestDto requestDto, TransactionType type, Card card, Mono<String> accountUid) {
        return accountUid.flatMap(value -> {
            Transaction transaction = Transaction.builder()
                    .paymentMethod(requestDto.getPaymentMethod())
                    .amount(requestDto.getAmount())
                    .currency(requestDto.getCurrency())
                    .createdAt(LocalDateTime.now())
                    .updatedAt(LocalDateTime.now())
                    .language(requestDto.getLanguage())
                    .notificationUrl(requestDto.getNotificationUrl())
                    .status(Status.IN_PROGRESS)
                    .transactionType(type)
                    .cardUid(card.getId())
                    .accountUid(value)
                    .build();
            return transactionRepository.save(transaction);
        });
    }

    public Mono<Transaction> findById(String id) {
        return transactionRepository.findById(id);
    }

    public Flux<Transaction> findByStatus(Status status, int size) {
        return transactionRepository.findByStatus(status, size);
    }

    public Mono<Transaction> update(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public Mono<Transaction> updateStatus(Transaction transaction, Status status) {
        transaction.setStatus(status);
        return transactionRepository.save(transaction).as(transactionalOperator::transactional);
    }

    public Mono<Transaction> findByIdForUpdate(Transaction transaction) {
        return transactionRepository.findByIdForUpdate(transaction.getId());
    }

    public Flux<Transaction> processing() {
        //TODO выгребать все без лимита
        return findByStatus(Status.IN_PROGRESS, 17)
                .switchIfEmpty(Mono.error(new TransactionInProgressNotFound("No transaction with status IN_PROGRESS", "WAITING FOR TRANSACTIONS IN PROGRESS")))
                .flatMap(transaction -> {
                    return updateStatus(transaction, getRandomSuccessOrFailedStatus())
                            .flatMap(changedTransaction -> {
                                if (changedTransaction.getStatus().equals(Status.SUCCESS)){
                                    return accountService
                                            .changeBalance(changedTransaction.getAccountUid(), changedTransaction.getAmount())
                                            //этот вариант пропускает поток дальше из-за чего success всем трем транзакциям (если не сделать перехвать дальше с onErrorContinue)
                                            .doOnError(RuntimeException.class, ex -> log.error("ПОЙМАЛИ ОШИБКУ В АККАУНТЕ {}", ex.toString()))
                                            .doOnError(SQLException.class, ex -> log.error("ПОЙМАЛИ SQL-ОШИБКУ В АККАУНТЕ {}", ex.toString()))
                                            .onErrorResume(RuntimeException.class, ex -> Mono.error(new RuntimeException("ПОЙМАЛИ ОШИБКУ В АККАУНТЕ")))
                                            //делаю map() чтобы возвращать не аккаунт а транзакцию, которую мне нужно обратать после transactionalOperator::transactional
                                            .map(smth -> changedTransaction);
                                } else if(changedTransaction.getStatus().equals(Status.FAILED)) {
                                    return cardService.payback(changedTransaction.getAmount(), changedTransaction.getCardUid(), changedTransaction)
                                            .map(smth -> changedTransaction);
                                }
                                return Mono.error(new RuntimeException("changing error"));
                            })
                            .as(transactionalOperator::transactional);
                })
                //TODO СПРОСИТЬ можно ли два друг за другом doOnError ставить
                .doOnError(RuntimeException.class, ex -> log.error("ПОЙМАЛИ ОШИБКУ В ТРАНЗАКЦИИ {}", ex.toString()))
                .doOnError(SQLException.class, ex -> log.error("ПОЙМАЛИ SQL-ОШИБКУ В ТРАНЗАКЦИИ {}", ex.toString()))
                //onErrorResume тут не работает,  нужно именно пропустить эту ошибку
                .onErrorContinue((err, i) -> {log.warn("onErrorContinue show message {}", err.getMessage());})
                ;
    }
}


