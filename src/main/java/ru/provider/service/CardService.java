package ru.provider.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Mono;
import ru.provider.dto.CardDto;
import ru.provider.dto.CustomerDto;
import ru.provider.entity.Card;
import ru.provider.entity.Transaction;
import ru.provider.mapper.CardMapper;
import ru.provider.repository.CardRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardService {

    private final CardRepository cardRepository;
    private final CardMapper cardMapper;
    private final CustomerService customerService;
    private final TransactionalOperator transactionalOperator;

    public Mono<Boolean> isCardExists(CardDto cardDto) {
        return cardRepository.findAll()
                .map(Card::getCardNumber)
                .hasElement(cardDto.getCardNumber());
    }

    public Mono<Card> findByCardNumber(Long cardNumber) {
        return cardRepository.findByCardNumber(cardNumber);
    }

    public Mono<Boolean> isCardsCustomerExists(Long cardNumber) {
        return findByCardNumber(cardNumber)
                .mapNotNull(Card::getCustomerUid)
                .hasElement();
    }


    public Mono<Card> save(CardDto cardDto) {
        log.info("cardService save method with dto " + cardDto.toString());
        return cardRepository.save(cardMapper.map(cardDto));
    }

    public Mono<Card> saveIfNotExists(CardDto cardData, CustomerDto customer) {
        return cardRepository.findByCardNumber(cardData.getCardNumber())
                .switchIfEmpty(Mono.defer(() -> {
                            return customerService.save(customer)
                                    .flatMap(savedCustomer -> {
                                        cardData.setCustomer(savedCustomer);
                                        cardData.setCustomerUid(savedCustomer.getId());
                                        return save(cardData);
                                    });
                        }
                ));

    }

    public Mono<Void> isEnoughMoneyOnCard(BigDecimal amount, Mono<Card> cardMono) {
        return cardMono
                .map(Card::getBalance)
                .log("in method isEnoughMoneyOnCard")
                .switchIfEmpty(Mono.error(new IllegalStateException("This Card without money")))
                .filter(balance -> balance.compareTo(amount) >= 0)
                .switchIfEmpty(Mono.error(new IllegalStateException("This card doesn't have enough money")))
                .then();
    }

    private Mono<Card> getCardByVerifiedBalance(BigDecimal amount, Mono<Card> cardMono) {
        return cardMono
                .log("in method getCardByVerifiedBalance")
                .filter(card -> card.getBalance().compareTo(amount) >= 0)
                .switchIfEmpty(Mono.error(new IllegalStateException("This card doesn't have enough money")));
    }

    public Mono<Card> updateBalance(BigDecimal amount, Card cardData) {
        return Mono.just(cardData)
                .log("in method updateBalance")
                .flatMap(card -> {
                    BigDecimal balance = card.getBalance().subtract(amount);
                    card.setBalance(balance);
                    return cardRepository.save(card);
                });
    }

    public Mono<Card> payback(BigDecimal amount, Long cardId, Transaction transaction) {
        return cardRepository.findByIdForUpdate(cardId)
                //если не делать for update то следующая транзакция успевает переписать предыдущую, проблема потерянной записи
                .flatMap(card -> {
                    BigDecimal balance = card.getBalance().add(amount);
                    log.info("paybalance amount = " + balance.longValue() + " tr id = " + transaction.getCardUid() + " cardID = " + cardId);
                    card.setBalance(balance);
                    return cardRepository.save(card);
                })
                .as(transactionalOperator::transactional);
    }

    private Mono<Card> checkCorrectLengthOfCardNumber(Mono<Card> cardMono) {
        return cardMono
                .log("in method isCorrectLengthOfCurdNumber")
                .filter(card -> Math.ceil(Math.log10(card.getCardNumber())) == 16)
                .switchIfEmpty(Mono.error(new IllegalStateException("Wrong card number")));
    }

    public Mono<Card> verify(BigDecimal amount, Mono<Card> cardMono) {
        return checkCorrectLengthOfCardNumber(cardMono)
                .then(verifyExpirationDate(cardMono))
                .then(getCardByVerifiedBalance(amount, cardMono));

    }

    private Mono<Card> verifyExpirationDate(Mono<Card> cardMono) {
        return cardMono.map(card -> {
            String[] split = card.getExpirationDate().split("/");
            int month = Integer.parseInt(split[0]);
            int year = Integer.parseInt(split[1]);

            //.minusYears(2000) чтобы сравнить текущий год и год на карте
            return (LocalDateTime.now().minusYears(2000).getYear() <= year &&
                    (LocalDateTime.now().getMonth().getValue() <= month));
        }).flatMap(result -> result ? cardMono : Mono.error(new IllegalStateException("This card is expired")));
    }

}
