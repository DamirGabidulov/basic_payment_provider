package ru.provider.service;

import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.ReactiveTransaction;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.reactive.TransactionalOperator;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.provider.dto.DepositRequestDto;
import ru.provider.entity.Account;
import ru.provider.entity.Currency;
import ru.provider.entity.Language;
import ru.provider.repository.AccountRepository;

import java.math.BigDecimal;
import java.util.function.BiFunction;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    private final DatabaseClient databaseClient;
    private final ReactiveTransactionManager transactionManager;
    private final TransactionalOperator transactionalOperator;

    public Flux<Account> getByTypeAndMerchantId(DepositRequestDto requestDto, String merchantId) {
        Currency currency = requestDto.getCurrency();
        //TODO должно вернуть mono
        return accountRepository.findAll()
                .filter(account -> account.getMerchantUid().equals(merchantId))
                .switchIfEmpty(Mono.error(new IllegalStateException("This merchant doesn't have any account")))
                .filter(account -> account.getCurrency().equals(currency))
                .switchIfEmpty(Mono.error(new IllegalStateException("Can't find account with this type of currency")))
                ;
    }

    public Mono<Account> getByTypeAndMerchantId(String merchantId, Currency currency) {
        return accountRepository.getByMerchantUidAndCurrency(merchantId, currency)
                .switchIfEmpty(Mono.error(new IllegalStateException("Can't find merchant or account with this type of currency")));
    }

    public Mono<Boolean> isEnoughMoneyOnAccount(BigDecimal amount, Mono<Account> accountMono) {
        return accountMono
                .map(Account::getAmount)
                .switchIfEmpty(Mono.error(new IllegalStateException("This account without money")))
                .filter(balance -> balance.compareTo(amount) >= 0)
                .hasElement();
    }

    public Mono<Account> changeBalance(String accountUid, BigDecimal amount) {
        return accountRepository.findByIdForUpdate(accountUid)
                .switchIfEmpty(Mono.error(new IllegalStateException("Can't find account")))
                .flatMap(account -> {
                    //эта проверка точно выбросит ошибку, так как есть пару транзакций с этим id в бд
                    if (!account.getId().equals("e584c208-0099-4d00-9702-084ebaa4c5f5")){
                        BigDecimal accountAmount = account.getAmount().add(amount);
                        account.setAmount(accountAmount);
                        return accountRepository.save(account);
                    } else {
                        return Mono.error(new IllegalStateException("EXCEPTION WITH ACCOUNT ID " + account.getId()));
                    }
                })
                .as(transactionalOperator::transactional);
    }

    public Mono<Account> findById(String accountId){
        return accountRepository.findById(accountId);
    }

    public Mono<Account> update(Account account){
        return accountRepository.save(account);
    }
}
