package ru.provider.config;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static org.springframework.security.web.server.ServerHttpBasicAuthenticationConverter.BASIC;

@Component
public class CustomWebFilter implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        String header = Objects.requireNonNull(exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION)).stream()
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can't find authorization header"));

        String credentials = (header.length() <= BASIC.length()) ? "" : header.substring(BASIC.length());
        String decoded = new String(Base64.getDecoder().decode(credentials));
        String[] parts = decoded.split(":", 2);
        if (parts.length != 2) {
            return Mono.empty();
        }
        exchange.getAttributes().put("merchantId", parts[0]);
        return chain.filter(exchange);
    }
}
