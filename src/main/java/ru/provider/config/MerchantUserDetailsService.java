package ru.provider.config;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.provider.entity.Merchant;
import ru.provider.repository.MerchantRepository;

@Service
@RequiredArgsConstructor
public class MerchantUserDetailsService implements ReactiveUserDetailsService {

    private final MerchantRepository merchantRepository;
    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return merchantRepository.findById(username)
                .switchIfEmpty(Mono.error(new IllegalStateException("can't find with this id")))
                .map(Merchant::toDetails);
    }
}
